sierra-config
=============


A little hack to configure (certain) sierra cell modems and show their
configuration.  I've put in the few things I occasionally needed:

* Modem reset (sometimes works when the firmware has crashed)
* Explicit connect to the infrastructure network (occasionally necessary
  for me)
* Setting the access method (GSM, UMTS, LTE; UMTS is probably dead
  almost everywhere these days, selecting one of the others can
  sometimes trade speed for connection reliability)
* Listing the available network providers.

Some of this will work with non-Sierra modems, some of it may only work
on close relatives of the Sierra EM7345.

Run ``python3 modem-config -h`` to see the options.


Installation
------------

This is a one-file python program without further dependencies. You can
run it as downloaded wherever you downloaded it, or you can make it
executable and copy it somewhere into your path.


License
-------

There's no copyright on this and hence you don't need a license.
Distributed under CC0.
